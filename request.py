import requests
import json

class Requests():
    def __init__(self,  token, userId, label):
        self.token = token
        self.userId = userId
        self.photosId = []
        self.listGroup = []
        self.listPostOnWall = []
        self.label = label
        self.countDelPhoto = 0
        self.countLeaveGroup = 0
        self.countDelPosts = 0

    #получить все фотографии пользователя
    def geAllPhotos(self):
        response = requests.get('https://api.vk.com/method/photos.getAll?owner_id=' + self.userId + '&v=5.52&access_token=' + self.token + '&expires_in=0')
        response = response.text
        response = json.loads(response)
        for photId in response['response']['items']:
            self.photosId.append(photId['id'])
            # print(self.photosId)

    #функция по удалению всех фотографии пользователя
    def deleteAllPhotos(self):
        self.geAllPhotos()
        for photoId in self.photosId:
            if(requests.get('https://api.vk.com/method/photos.delete?owner_id=' + self.userId + '&v=5.52&&photo_id=' + str(photoId) +'&access_token=' + self.token + '&expires_in=0')):
                self.countDelPhoto += 1
                print("succes delete: " + str(photoId))
                self.label.setText("Фотография id:" + str(photoId) + ", успешно удалена, общее количество удалленых фотографии: " + str(self.countDelPhoto))

    #получить id всех групп пользователя
    def getAllGroups(self):
        response = requests.get('https://api.vk.com/method/groups.get?user_id=' + self.userId + '&v=5.52&access_token=' + self.token + '&expires_in=0')
        response = response.text
        response = json.loads(response)
        print(response['response']['items'])
        for group in response['response']['items']:
            self.listGroup.append(group)
            # print(self.listGroup)

    #выйти из групп пользователя
    def leaveFromAllGropus(self):
        self.getAllGroups()
        for group in self.listGroup:
            requests.get('https://api.vk.com/method/groups.leave?group_id=' + str(group) +'&v=5.52&access_token=' + self.token + '&expires_in=0')
            self.countLeaveGroup += 1
            print("succes leave group: " + str(group))
            self.label.setText("Выход из группы(id):" + str(group) + ", общее количество покинутых групп: " + str(self.countLeaveGroup))

    #получить id всех постов на странице пользователя
    def getAllPostsOnWall(self):
        response = requests.get('https://api.vk.com/method/wall.get?owner_id=' + self.userId + '&v=5.52&access_token=' + self.token + '&expires_in=0')
        response = response.text
        response = json.loads(response)
        print(response['response']['items'])
        for post in response['response']['items']:
            self.listPostOnWall.append(post['id'])
            # print(self.listGroup)

    #удалить все посты со страницы пользователя
    def delAllPostsOnUserWall(self):
        self.getAllPostsOnWall()
        for post in self.listPostOnWall:
            requests.get('https://api.vk.com/method/wall.delete?owner_id=' + self.userId + '&post_id=' + str(post) + '&v=5.52&access_token=' + self.token + '&expires_in=0')
            self.countDelPosts += 1
            print("succes delete post(id): " + str(post))
            self.label.setText("Удален пост(id):" + str(post) + ", общее количество удаленных постов: " + str(self.countDelPosts))




