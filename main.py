# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'test7.ui'
##
## Created by: Qt User Interface Compiler version 5.14.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient, QDesktopServices)
from PySide2.QtWidgets import *

import sys
import request

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(800, 587)
        font = QFont()
        font.setItalic(False)
        MainWindow.setFont(font)
        self.apllication = QWidget(MainWindow)
        self.apllication.setObjectName(u"apllication")
        self.stackedWidget = QStackedWidget(self.apllication)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.stackedWidget.setGeometry(QRect(20, 0, 791, 571))
        self.page_3 = QWidget()
        self.page_3.setObjectName(u"page_3")
        self.lineEdit_3 = QLineEdit(self.page_3)
        self.lineEdit_3.setObjectName(u"lineEdit_3")
        self.lineEdit_3.setGeometry(QRect(200, 100, 371, 21))
        self.pushButton_5 = QPushButton(self.page_3)
        self.pushButton_5.setObjectName(u"pushButton_5")
        self.pushButton_5.setGeometry(QRect(310, 140, 141, 31))
        font1 = QFont()
        font1.setPointSize(15)
        font1.setBold(False)
        font1.setItalic(False)
        font1.setWeight(50)
        self.pushButton_5.setFont(font1)
        self.pushButton_5.setMouseTracking(False)
        self.pushButton_6 = QPushButton(self.page_3)
        self.pushButton_6.setObjectName(u"pushButton_6")
        self.pushButton_6.setGeometry(QRect(290, 300, 191, 41))
        self.lineEdit_4 = QLineEdit(self.page_3)
        self.lineEdit_4.setObjectName(u"lineEdit_4")
        self.lineEdit_4.setGeometry(QRect(200, 60, 371, 21))
        self.pushButton_7 = QPushButton(self.page_3)
        self.pushButton_7.setObjectName(u"pushButton_7")
        self.pushButton_7.setGeometry(QRect(290, 350, 191, 41))
        self.pushButton_8 = QPushButton(self.page_3)
        self.pushButton_8.setObjectName(u"pushButton_8")
        self.pushButton_8.setGeometry(QRect(290, 400, 191, 41))
        self.label = QLabel(self.page_3)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(0, 10, 791, 41))
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setTextInteractionFlags(Qt.TextEditable)
        self.pushButton = QPushButton(self.page_3)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(150, 240, 481, 23))
        self.pushButton.setStyleSheet(u"color:blue")
        self.pushButton_2 = QPushButton(self.page_3)
        self.pushButton_2.setObjectName(u"pushButton_2")
        self.pushButton_2.setGeometry(QRect(220, 200, 331, 23))
        self.pushButton_2.setStyleSheet(u"color:blue")
        self.stackedWidget.addWidget(self.page_3)
        self.page_4 = QWidget()
        self.page_4.setObjectName(u"page_4")
        self.stackedWidget.addWidget(self.page_4)
        MainWindow.setCentralWidget(self.apllication)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 21))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Помогатор VK", None))
        self.lineEdit_3.setText(QCoreApplication.translate("MainWindow", u"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 id \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f", None))
        self.pushButton_5.setText(QCoreApplication.translate("MainWindow", u"\u041f\u0420\u0418\u041d\u042f\u0422\u042c", None))
        self.pushButton_6.setText(QCoreApplication.translate("MainWindow", u"DELETE PHOTOS", None))
        self.lineEdit_4.setText(QCoreApplication.translate("MainWindow", u"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 token", None))
        self.pushButton_7.setText(QCoreApplication.translate("MainWindow", u"DELETE POSTS", None))
        self.pushButton_8.setText(QCoreApplication.translate("MainWindow", u"STOP TRACKING GROUPS", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" color:#00aa7f;\">Token \u0438 id \u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044f \u043f\u043e\u043b\u0443\u0447\u0435\u043d</span></p></body></html>", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"\u041f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u0435 user id(\u0410\u0434\u0440\u0435\u0441 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b->\u0438\u0437\u043c\u0435\u043d\u0438\u0442\u044c)", None))
        self.pushButton_2.setText(QCoreApplication.translate("MainWindow", u"\u041f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f token'\u0430 (VK API)", None))
    # retranslateUi




def setText():
    # window.ui.lineEdit.setText("test2")
    request2 = request.Request(window.ui.lineEdit)
    request2.deleteAllPhotos()


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)




app = QApplication(sys.argv)

window = MainWindow()
#основной код здесь

#основная функция
def mainFun():
    hideOnStart()
    checkClickButtons()
    checkClickOnLick()

#функция проверки наличия токена и id
def checkTokenId():
    if window.ui.lineEdit_4.text() != "Введите token" and window.ui.lineEdit_3.text() != "Введите id пользователя":
        window.ui.label.show()
        window.ui.pushButton_6.show()
        window.ui.pushButton_7.show()
        window.ui.pushButton_8.show()
        window.ui.pushButton_2.hide()
        window.ui.pushButton.hide()

#объекты, которые нужно скрыть при старте
def hideOnStart():
    if window.ui.lineEdit_4.text() == "Введите token" and window.ui.lineEdit_3.text() == "Введите id пользователя":
        window.ui.label.hide()
        window.ui.pushButton_6.hide()
        window.ui.pushButton_7.hide()
        window.ui.pushButton_8.hide()

#проверка нажатия кнопок
def checkClickButtons():
    window.ui.pushButton_5.clicked.connect(checkTokenId)
    window.ui.pushButton_6.clicked.connect(reqDeletephoto)
    window.ui.pushButton_8.clicked.connect(reqLeaveAllGroups)
    window.ui.pushButton_7.clicked.connect(reqDeleteAllPoostsOnUserWall)

#проверка нажатия на ссылки
def checkClickOnLick():
    window.ui.pushButton_2.clicked.connect(openGitIo)
    window.ui.pushButton.clicked.connect(openSettingVk)

#открытие страницы с получением токена
def openGitIo():
    QDesktopServices.openUrl(QUrl("https://vkhost.github.io/"))

#открытие страницы с получения User id
def openSettingVk():
    QDesktopServices.openUrl(QUrl("https://vk.com/settings"))

#функция удаления всех фотографии
def reqDeletephoto():
    req = request.Requests(window.ui.lineEdit_4.text(), window.ui.lineEdit_3.text(), window.ui.label)
    req.deleteAllPhotos()

#функция выхода из всех групп
def reqLeaveAllGroups():
    req = request.Requests(window.ui.lineEdit_4.text(), window.ui.lineEdit_3.text(), window.ui.label)
    req.leaveFromAllGropus()

def reqDeleteAllPoostsOnUserWall():
    req = request.Requests(window.ui.lineEdit_4.text(), window.ui.lineEdit_3.text(), window.ui.label)
    req.delAllPostsOnUserWall()

#запуск основной функций
mainFun()


window.show()
# Получить текст и lineEdit
# print(window.ui.lineEdit.text())

sys.exit(app.exec_())

